Load Env
========

A role to load a shell env in ansible variable.

May be usefull to load conda or spack env.


Variables
---------

[Role Variables](defaults/main.yml)


Example Simple Test Playbook
----------------------------

[Test Playbook](molecule/default/playbook.yml)

Example using R in a Conda environment to install R package
---------------------------------------------

```yaml
- name: Install R package
  hosts: r-env
  vars:
    r_version_35: true
    r_base_path: "{{ conda_env_home }}/{{ r_env }}"
    r_packages_lib: "{{ r_base_path }}/lib/R/library"
  tasks:
    - name: Include Load Env
      include_role:
        name: load-env
      vars:
        env_context_launcher: "/shared/mfs/data/dev/software/miniconda/bin/conda run -n {{ r_env }}"
      when: r_env is defined
    - name: Show Vars content
      debug:
        var: loaded_env
    - name: Include ansible-r/packages
      include_role:
        name: ansible-r
        tasks_from: packages
      args:
        apply:
          environment: "{{ loaded_env }}"
      when: loaded_env is defined
```

With a requirement file like:

```yaml
# this is for r package installation
- src: https://github.com/IFB-ElixirFr/ansible-r.git
  name: "ansible-r"
  scm: git
  version: bioconductor-3.8
# this for loading env
- src: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-load-env.git
  name: "load-env"
  scm: git
  version: 0.0.2
```



Lauched like:

```bash
ansible-galaxy install -vvv --force -r requirements.yml
ansible-playbook -i development playbook_r-install-package.yml -e 'r_env=r-3.5.1'
```

License
-------

[License](LICENSE)
