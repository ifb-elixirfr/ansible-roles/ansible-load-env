<a name="unreleased"></a>
## [Unreleased]


<a name="0.2.0"></a>
## [0.2.0] - 2020-04-17
### Load
- load less env on saved file too

### Remove
- remove generated file from source control

### Try
- try to not import env before bash
- try (harder) to load less env (to avoid module load)
- try to use explicite shell command
- try to load less env with disable login of bash config file

### Reverts
- try (harder) to load less env (to avoid module load)

### Merge Requests
- Merge branch 'try_to_load_less_env' into 'master'


<a name="0.1.0"></a>
## [0.1.0] - 2020-04-16
### Add
- add save shell env file on remote host

### Disable
- disable save file by default

### Merge Requests
- Merge branch 'add_save_env_file' into 'master'


<a name="0.0.2"></a>
## [0.0.2] - 2020-03-16
### Add
- Add a precise example about R with a conda env

### Remove
- remove debian and fedora as it does not work out of the box


<a name="0.0.1"></a>
## 0.0.1 - 2020-03-16
### First
- First Commit :)


[Unreleased]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-load-env/compare/0.2.0...HEAD
[0.2.0]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-load-env/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-load-env/compare/0.0.2...0.1.0
[0.0.2]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-load-env/compare/0.0.1...0.0.2
